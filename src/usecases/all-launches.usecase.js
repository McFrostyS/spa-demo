import { LaunchesRepository } from '../repositories/launches.repository'
import { Launch } from '../model/launch'

export class AllLaunchesUsecase {
  static async execute() {
    const repository = new LaunchesRepository()
    const launches = await repository.getAllLaunches()
    if (launches) {
      const newLaunchesModel = launches.map((launch) => new Launch(launch))
      return newLaunchesModel
    }
  }
}
