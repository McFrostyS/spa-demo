import { LitElement, html, css } from 'lit'
import { AllLaunchesUsecase } from '../usecases/all-launches.usecase'
import './launch.component'

export class LaunchesComponent extends LitElement {
  static properties = {
    launches: { type: Array }
  }

  static styles = css`
    :host {
      display: block;
      padding: 16px;
      font-family: 'Roboto', sans-serif;
      color: #333;
    }

    h1 {
      color: #212121;
      font-size: 24px;
      margin-bottom: 16px;
    }

    ul {
      list-style: none;
      padding: 0;
      margin: 0;
      display: grid;
      grid-template-columns: repeat(4, 1fr);
      gap: 16px;
    }

    li {
      background-color: #fff;
      border-radius: 8px;
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
      padding: 16px;
    }
  `

  constructor() {
    super()
    this.launches = []
  }

  async connectedCallback() {
    super.connectedCallback()
    this.launches = await AllLaunchesUsecase.execute()
  }

  render() {
    return html`
      <h1>Launches</h1>
      <ul>
        ${this.launches.map(
          (launch) =>
            html`<li><launch-component .launch=${launch}></launch-component></li>`
        )}
      </ul>
    `
  }
}

customElements.define('launches-component', LaunchesComponent)
