import { LitElement, html, css } from 'lit'

export class LaunchComponent extends LitElement {
  static properties = {
    launch: { type: Object }
  }

  static styles = css`
    div {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      margin-bottom: 16px;
      font-family: 'Roboto', sans-serif;
      height: 400px;
    }

    img {
      display: block;
      height: 200px;
      object-fit: cover;
      margin: 0 auto;
    }

    h2 {
      margin: 0;
      color: #212121;
      font-size: 24px;
      display: inline;
    }

    p {
      margin: 4px 0;
      color: #757575;
      line-height: 1.6;
    }

    .status {
      margin-left: 8px;
      font-size: 20px;
      font-weight: bold;
    }

    .status.success {
      color: green;
    }

    .status.failure {
      color: red;
    }

    .description {
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .readMore {
      background-color: #007bff;
      color: #fff;
      border: none;
      border-radius: 4px;
      padding: 8px 16px;
      width: fit-content;
      text-decoration: none;
      font-weight: bold;
      margin-top: 16px;
      margin-left: auto;
      margin-right: auto;
    }

    .readMore:hover {
      background-color: #0056b3;
      cursor: pointer;
    }
  `

  constructor() {
    super()
    this.launch = {}
  }

  render() {
    return html`
      <div>
        <header>
          <h2>${this.launch.missionName}</h2>
          <span class="status ${this.launch.launchSuccess ? 'success' : 'failure'}">
            ${this.launch.launchSuccess ? '✔' : '✖'}
          </span>
          <p class="launchDate">${this.launch.launchDate}</p>
        </header>
        <p class="description">${this.launch.details}</p>
        <img src="${this.launch.patch}" alt="${this.launch.missionName}" />
        <a class="readMore" href="${this.launch.links.articleLink}" target="_blank"
          >Read more</a
        >
      </div>
    `
  }
}

customElements.define('launch-component', LaunchComponent)
