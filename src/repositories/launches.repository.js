export const API_URL = 'https://api.spacexdata.com/v3/launches'

export class LaunchesRepository {
  async getAllLaunches() {
    const response = await fetch(API_URL)
    const data = await response.json()
    return data
  }

  async getLaunchById(id) {
    const response = await fetch(`${API_URL}/${id}`)
    const data = await response.json()
    return data
  }
}
