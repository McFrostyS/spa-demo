export class Launch {
  constructor(data) {
    this.flightNumber = data.flight_number
    this.missionName = data.mission_name
    this.patch = data.links.mission_patch
    this.launchYear = data.launch_year
    this.launchDate = data.launch_date_utc.split('T')[0] // Formateo de fecha
    this.details = data.details
    this.launchSuccess = data.launch_success
    this.links = data.links
  }
}
