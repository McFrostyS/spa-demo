import '../components/launches.component'

export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
      <h1>SpaceX 🚀</h1>
      <launches-component></launches-component>
      `
  }
}

customElements.define('home-page', HomePage)
