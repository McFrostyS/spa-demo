/// <reference types="cypress" />

describe('Posts Management', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/')
    cy.get('[href="/posts"]').click()
  })

  it('should create a post', () => {
    cy.get('#post-title').type('Prueba')
    cy.get('#post-content').type('Prueba')
    cy.get('.addBtn').click()
    cy.get(':nth-child(1) > post-ui > a > #title').should('have.text', 'Prueba')
  })

  it('should delete a post', () => {
    cy.get('div > :nth-child(6)').click()
    cy.get(':nth-child(1) > post-ui > a > #title').click()
    cy.get('div > :nth-child(8)').click()
    cy.get(':nth-child(1) > post-ui > a > #title').should('not.have.text', 'Prueba')
  })

  it('should update a post', () => {
    cy.get('div > :nth-child(6)').click()
    cy.get(':nth-child(1) > post-ui > a > #title').click()
    cy.get('#post-title').clear().type('New Prueba')
    cy.get('div > :nth-child(7)').click()
    cy.get('.logo').click()
    cy.get('[href="/posts"]').click()
    cy.get(':nth-child(1) > post-ui > a > #title').should('have.text', 'New Prueba')
  })
})
